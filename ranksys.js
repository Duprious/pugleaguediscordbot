const r = require('openskill');
const d = require('./dbreqs.js');
const { MessageEmbed } = require('discord.js');
const u = require("./utils.js");

function getLeague(player, authorId, leagueId){
	d.getRank(authorId, leagueId, function(err, row) {
		league = {};
		if(typeof row === 'undefined'){
			league.rank = new r.rating();
			league.wins = 0;
			league.losses = 0;
			league.draws = 0;
			league.streak = 0;
		}else{
			league.rank = new r.rating({mu:row.mu,sigma:row.sig});
			league.wins = row.wins;
			league.losses = row.losses;
			league.draws = row.draws;
			league.streak = row.streak
		}
		player.leagues[leagueId] = league;
	});
}

function getRank(guild, message, season=true, player=""){
	var leagueid;
	var placementgames;
	if(season){
		var pickup = null;
		for(p in guild.pickups){
			pickup = guild.pickups[p]
			break; //we just need any
		}
		leagueid = pickup.leagueid;
		placementgames = pickup.placementgames;
	}else{
		leagueid = guild.regionid;
		placementgames = guild.placementgames;
	}
	d.getDetailedRank(message.author.id, player, leagueid, placementgames, function(row=null) {
		//if rows blank
		if(row==null){
			u.reply(message, "No player found");
			return;
		}
		nickname = "";
		if(player.length>1) nickname = player;
		else {
			nickname = message.member.nickname;
			if(nickname==null) nickname = message.author.username;
		}
		totalgames = row.wins+row.losses+row.draws;
		position = "Outside Top 500";
		if(row.hasOwnProperty('position')) position = row.position;
		desc = "";
		if(totalgames>placementgames) desc = `${u.stringRank({mu:row.mu, sigma:row.sig})}`;
		else desc = `${totalgames}/${placementgames} Placement Games`;
		const outEmbed = new MessageEmbed()
			//.setColor('#0099ff')
			.setTitle(nickname)
			.setDescription(desc)
			.addFields(
				{ name: 'Position', value:""+position, inline: true},
				{ name: 'W/L/D (Win%)', value: `${row.wins}/${row.losses}/${row.draws} (${Math.round(100*(row.wins/(row.wins+row.losses)))}%)`, inline:true},
				{ name: 'Matches Played', value: ""+totalgames, inline: true },
				{ name: 'Current Streak', value: ""+row.streak, inline: true },
			)
			.setTimestamp();

		message.channel.send({ embeds: [outEmbed] });
		console.log("outputted embed");
	});
}

//function getLB(guild, message, season=true, number=1){
function getLB(message, leagueid, number=1){
	//TODO placement games, not sure how I want to tackle it
	//TODO curren't doesn't do anything besides seasonal current
	placementgames = 0;
	if(isNaN(number)) number = 1;//just in case
	d.getLB(leagueid, placementgames, number, function(er, rows) {
		u.reply(message, u.printLB(rows, number), `Printed LB`);
	});
}

/* score = [team1score,team2score]
 * leagueid
 * regionid (just the region's league id
 * all the players in team 1 & 2 (just contians their ids for reference in match.players)
 * match.players (contains all the actual player info)
 * how many placementgames ### placement games should be league specific
 *
 */

// maybe abstract so its not t1, t2, but instead t[0],t[1].
function rankMatch(leagueid, regionid, match, score){
	outMsg = "";
	l = match.t1.length;
	nstats = {};
	//new ranks, but its actually a whole new player.league
	nstats[leagueid] = [{},{}]; //leagueid: [team1, team2]
	nstats[regionid] = [{},{}]; //where team1/2 is an object of player ranks

	//TODO change struct of Match so you don't have to reconstruct the ranks
	oranks = {};
	oranks[leagueid] = [new Array(l), new Array(l)];
	oranks[regionid] = [new Array(l), new Array(l)];
	for(lg in oranks){
		for(let pl=0;pl<l;pl++){
			id1 = match.t1[pl];
			id2 = match.t2[pl];
			oranks[lg][0][pl] = match.players[id1].leagues[lg].rank;
			oranks[lg][1][pl] = match.players[id2].leagues[lg].rank;
		}
	}
	nranks = {};
	if(score[0]==score[1]){
		nranks[leagueid] = r.rate(oranks[leagueid], {score:[1,1]} );
		nranks[regionid] = r.rate(oranks[regoinid], {score:[1,1]} );
	}else{
		nranks[leagueid] = r.rate(oranks[leagueid], {score:score} );
		nranks[regionid] = r.rate(oranks[regoinid], {score:score} );
	}

	for(lg in cranks){
		for(let tm=0;tm<2;tm++){
			for(let pl=0;pl<l;pl++){
				console.log(lg);
				console.log(tm);
				console.log(pl);
				plId = null;
				if(tm==0) plId = match.t1[pl];
				else plId = match.t2[pl];
				player = match.players[plId];

				draws = player.leagues[lg].draws;
				if(score[0]==score[1]) draws++;

				streak = player.leagues[lg].streak;
				if(streak>0&&score[tm]==1) streak++;
				else if (streak<0&&score[tm]==0) streak--;
				else streak = score[tm]-score[1-tm];

				nstats[lg][tm][plId] = {
					rank: nranks[lg][tm][pl],
					wins: player.leagues[lg].wins + score[tm],
					losses: player.leagues[lg].losses +score[1-tm],
					draws: draws,
					streak: streak
				}
				outMsg +=`\n${player.nickname}: `;
				//outMsg +=u.stringUpdateRank(original rank, new rank, 1);

				//gonna need to rewrite d.reportMatch
			}
		}
	}
		//need to return an outmessage
}

module.exports = {
	getLeague,
	getRank,
	getLB
}
