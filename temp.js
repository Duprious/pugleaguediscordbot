var ranks;
leagueid = guild.pickups[p].leagueid;
regionid = guild.regionid;
lid = [leagueid,regionid];
t1 = match.t1;
t2 = match.t2;

l = match.t1.length;
t1r = new Array(l);
t2r = new Array(l);
t1l = new Array(l);
t2l = new Array(l);
players = match.players;
placementgames = guild.pickups[p].placementgames;
for(let i=0;i<l;i++){
	p1 = t1[i];
	t1r[i] = players[p1].leagues[regionid].rank;
	t1l[i] = players[p1].leagues[leagueid].rank;
	p2 = t2[i];
	t2r[i] = players[p2].leagues[regionid].rank;
	t2l[i] = players[p2].leagues[leagueid].rank;
	for(let j=0;j<2;j++){
		players[p1].leagues[lid[j]].wins += score[0];
		players[p1].leagues[lid[j]].losses += score[1];
		if(score[0]==score[1]) players[p1].leagues[lid[j]].draws++;
		if(players[p1].leagues[lid[j]].streak>0 && score[1]==0) players[p1].leagues[lid[j]].streak+=score[0];
		else if(players[p1].leagues[lid[j]].streak<0 &&score[0]==0) players[p1].leagues[lid[j]].streak-=score[1];
		else players[p1].leagues[lid[j]].streak=score[0]-score[1];

		players[p2].leagues[lid[j]].wins += score[1];
		players[p2].leagues[lid[j]].losses += score[0];
		if(players[p2].leagues[lid[j]].streak>0 && score[0]==0) players[p2].leagues[lid[j]].streak+=score[1];
		else if(players[p2].leagues[lid[j]].streak<0 &&score[1]==0) players[p2].leagues[lid[j]].streak-=score[0];
		else players[p2].leagues[lid[j]].streak=score[1]-score[0];
	}
}
[o1r, o2r] = [null,null];
[o1l, o2l] = [null,null];
let promise = new Promise(function(resolve,reject) {
	if(result=='d'){//it doesn't like draws with score [0,0] for some reason, need openskill to patch
		[o1r, o2r] = r.rate([t1r, t2r], {score:[1,1]});
		[o1l, o2l] = r.rate([t1l, t2l], {score:[1,1]});
		resolve();
	}else{
		[o1r, o2r] = r.rate([t1r, t2r], {score:score});
		[o1l, o2l] = r.rate([t1l, t2l], {score:score});
		resolve();
	}
});
promise.then(function() {
	outMessage+=`\n\n[Team1]`;
	for(let i=0;i<l;i++){
		p1 = t1[i];
		l1 = o1r[i];

		outMessage +=`\n${players[p1].nickname}: `;
		placed = (players[p1].leagues[leagueid].wins+players[p1].leagues[leagueid].losses+players[p1].leagues[leagueid].draws)-placementgames;
		outMessage+=u.stringUpdateRank(players[p1].leagues[leagueid].rank, l1, placed);

		players[p1].leagues[leagueid].rank = l1;
		players[p1].leagues[regionid].rank = o1r[i];
	}
	outMessage+=`\n[Team2]`;
	for(let i=0;i<l;i++){
		p2 = t2[i];
		l2 = o2r[i];

		outMessage +=`\n${players[p2].nickname}: `;
		placed = (players[p2].leagues[regionid].wins+players[p2].leagues[regionid].losses+players[p2].leagues[regionid].draws)-placementgames;
		outMessage+=u.stringUpdateRank(players[p2].leagues[leagueid].rank, l2, placed);

		players[p2].leagues[leagueid].rank = l2;
		players[p2].leagues[regionid].rank = o2r[i];
	}
	ranks = [t1r,t2r, t1l,t2l];
	d.reportMatch(match, s_totalgames, s_draws, ranks, leagueid, regionid);
	u.reply(message, outMessage, `Ended Match ${m}, result: ${result}`, true);

