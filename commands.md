## Legend

**command** "required variable" [optional variable] - description of the command  
    --flags/-f [optional variable for flag] - description of the flag.  

**NOTE: flag variables must come before all other variables, see example below**

For example:  
**!rank** [name] - gets you your rank [unless you specify another players name]  
    --max/-m - prints their maximum rank instead of their current rank  
    --season/-s "season Number" - prints their rank for the given season  

**the following would be valid rank commands:**  
"!rank" - this would just produce **your current rank**  
"!rank -m" - this would produce your **MAXIMUM** rank  
"!rank --max NizzQ" - this would produce **NizzQ's (a player)** maximum rank  
"!rank -s 2" - this would produce **your final(current)** rank **from season 2**  
"!rank -sm 2 NizzQ" - this would produce **NizzQ's maximum rank he got during season 2**.  
etc.  

# Matchmaking

**++/!add/!q** [pickup] - adds you to the queue [for that pickup]  

**--/!remove** - removes you from all queues.  

**!who** - prints queues for all current pickups  
    --global/-g - shows queues from all regions  
    --region/-r "region" - shows queues in specified region

**!matches** - prints all current matches  
    --state/-S - shows what state each match is in  
    --global/-g - shows matches from all regions  
    --region/-r "region" - shows matches in a given region  
    --small/-s - just prints match ids  

**!match/!teams/!map** [match id] - prints current match [or of given matchid]  

**!rl** - report loss (if you are captain and you lost, you report it with !rl)  

**!rc** - report cancel (both team captains need to !rc for a match to cancel)  

**!rd** - report draw (both team captains need to !rd for a match be be resolved in a draw)  

**!pick** "@player" [,@Player] - pick a player for your team during match picking stage. You can specify your next pick as well.  
  typing "!pick -a" or "!pick --auto" will automatically pick who the bot deems best for your team.  

# Ranked System

Our ranking system is powered by OpenSkill (works similar to that of TrueSkill)

**!rank** [player] - prints your rank [or for another player].  
     -m/--max - prints max rank achieved instead of current  
     -c/--current - prints current rank (this is already default)  
     -p/--player "player" - specifys a specific player  
     -g/--global - view from all regions, not just this discord  
     -a/--alltime - view from all time rank (from all games you have ever played), not just this current season  
    -r/--region "region" - view rank on a specific region  
    -d/--discordid "discordid" - use a discordid instead of specifying a players name (good for staff when someone has a name that is difficult to type)  
    -l/--leagueid "leagueid" - view rank for a given league specified by id.  
    -s/--season "name/number" - view rank of a given season.  

**!lb/!leaderboard** [page] - prints leaderboard (page 1 by default)  
    -m/--max - leaderboard for max rank, not current  
    -c/--current - current rank not max (this is already default)  
    -p/--page "page#" - specifys a specific page  
    -g/--global - view from all regions, not just this discord  
    -a/--alltime - view from all time ranks (from all games you have ever played), not just this current season  
    -r/--region "region" - view leaderboard on a specific region  
    -l/--leagueid "leagueid" - view leaderboard for a given league specified by id.  
    -s/--season "name/number" - view leaderboard of a given season.  

**!ranks/!ranks_table** - see what ranks correlate to what elo  

# Other commands

**!ping** - returns how long it takes bot to recieve your message  

**!help/!commands** - prints this page  

**!expire**  

**!lastgame**  

**!stats**  

**!maps** [pickup] - see current map pool (by default, for the whole discord). [If a pickup is specified, you will see the map pool for that pickup]  

# Administration  

**!set** "setting" "value" - Sets the setting to the value provided. By default, this will set the setting for the whole discord, not the pickup.  
    **NOTE: A full list of settings is contained in the section below**  
    --default/-d - sets for whole discord server  
    --pickup/-p [pickup] - sets setting for the pickup. Sepcifying which pickup is requred if there is more than one.  

**!createpickup/!addpickup** "name of pickup" - create a new pickup  

**!noadd/!ban** "@player" [reason] - noadds for a default of 2 hours. Use flags to specify time  
  **NOTE: do not mix flags. Only one flag is allowed. If you want to do a ban of 2 hours and 30 mins, just do "-m 150"**  
  **NOTE: if a given player already has a noadd, then the noadd is updated. This can be used to update the length of their ban, or the reason they were banned. However, it will change the "issuer" of the noadd to the person who last updated the noadd**  
    --permanent/-p - permanent ban  
    --hour(s)/-h "hours" - specify  time in hours  
    --minute(s)/-m "minutes" - specify time in minutes  
    --day(s)/-d "days" - specify time in days  
    --month(s)/-M "months" - specify time in months  
    --years/-y "years" - specify time in years  
    --reason/-r "reason" - reason for ban  

**!noadds** [Player] [page#] - view all noadds, [of a certain player, or a differnt page of noadds]  
    --player/-p "player" - specify player  
    --discordid/-d "discordid" - specify player by discordid  
    --historic/-h - view all noadds, including ones previously expired.  
    --global/-g - view noadds accross all regoins/servers.  
    --region/-r "region" - view noadds for a specific region  
    --simple/-s - just display how many per person, and when it expires  

**!forgive** "@Player" - expires a given players noadd.  

**!setmatch/!rw** "matchid" "1|2|d|c"  
    1 = team 1 won  
    2 = team 2 won  
    d = draw  
    c = cancel  

**!cfg/!config** [pickup] - View configuration **via dms**. Defaults to discord config [but can specify a pickup]  
    --pickup/-p "pickup" - specify a pickup  
    --region/-r "region" - view config from another region/server  

**!close/!open** [pickup] - closes and opens the queue for all pickups [unless specific pickup specified]. All players are removed from the queue when it closes, but matches and all other commands will work as normal. Good for when you need to restart bot, or make changes, or reset season, but people keep q'ing.  

**!newseason** "name/#" - end the current season, and begin a new one. Names cannot have spaces, and are suggested to be as simple as possble. Just do something like "2" instead of "KPC-PUG-SEASON-2:YEAR-OF-THE-DRAGON!"  

# Settings

to be written
