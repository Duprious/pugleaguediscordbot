var sqlite3 = require('sqlite3');
var db = new sqlite3.Database("maindb.sqlite3");

db.serialize(function() {
	db.run(`CREATE TABLE players (
		discordid TEXT UNIQUE PRIMARY KEY,
		krunker TEXT UNIQUE,
		nickname TEXT NOT NULL,
		region INTEGER,
		country TEXT,
		premium INTEGER DEFAULT 0 NOT NULL,
		punished INTEGER DEFAULT 0 NOT NULL,
		classes INTEGER DEFAULT 0 NOT NULL,
		maxrankmu REAL,
		maxrankinfo TEXT,
		team INTEGER
		)`);
	db.run(`CREATE TABLE stats (
		nextmatchid INTEGER NOT NULL,
		nextplayerid INTEGER NOT NULL,
		nextleagueid INTEGER NOT NULL,
		nextpickupid INTERER NOT NULL
		)`);
	db.run(`INSERT INTO stats (nextmatchid, nextplayerid, nextleagueid, nextpickupid)
		VALUES (1,1,1,1)`);
	db.run(`CREATE TABLE discords (
		guildid TEXT UNIQUE PRIMARY KEY,
		regionid INTEGER,
		whiterole TEXT,
		blackrole TEXT,
		mappool TEXT DEFAULT '',
		pickteams INTEGER DEFAULT 0,
		ranked INTEGER DEFAULT 0,
		teamsize INTEGER DEFAULT 4,
		placementgames INTEGER DEFAULT 10,
		mappickbans INTEGER DEFAULT 0,
		openhours TEXT,
		bannedclasses INTEGER DEFAULT 0,

		overlordid TEXT NOT NULL,
		adminrole TEXT,
		modrole TEXT,
		readytime INTEGER DEFAULT 0,
		assignblackrole INTEGER DEFAULT 0,
		channelids TEXT,
		verifychannel TEXT,
		promotedelay INTEGER DEFAULT 1800,
		waitingroom TEXT
		)`);
	db.run(`CREATE TABLE pickups (
		name TEXT,
		pickupid INTEGER UNIQUE PRIMARY KEY,
		discordid TEXT,
		leagueid INTEGER,
		whiterole TEXT,
		blackrole TEXT,
		mappool TEXT,
		pickteams INTEGER,
		ranked INTEGER,
		teamsize INTEGER,
		placementgames INTEGER,
		mappickbans INTEGER,
		openhours TEXT,
		bannedclasses INTEGER,

		s_totalgames INTEGER DEFAULT 0,
		s_draws INTEGER DEFAULT 0,
		s_mostwonplayer REAL,
		s_mostwonplayerinfo TEXT,
		s_mostwonteam REAL,
		s_mostwonteaminfo TEXT,
		s_mostlostplayer REAL,
		s_mostlostplayerinfo TEXT,
		s_mostlostteam REAL,
		s_mostlostteaminfo TEXT,
		s_fairestgame REAL,
		s_fairestgameinfo TEXT,
		s_unfairestgame REAL,
		s_unfairestgameinfo TEXT,
		s_upset REAL,
		s_upsetinfo TEXT,
		s_highestgame REAL,
		s_highestgameinfo TEXT,
		s_lowestgame REAL,
		s_lowestgameinfo TEXT,
		s_longestwinstreak INTEGER,
		s_longestlosestreak INTEGER
		)`);
	db.run(`CREATE TABLE infractions (
		discordid TEXT NOT NULL,
		guildid TEXT NOT NULL,
		issuetime INTEGER NOT NULL,
		duration INTEGER NOT NULL,
		reason TEXT,
		issuer TEXT NOT NULL,
		expired INTEGER NOT NULL,
		forgiven TEXT
		)`);
	db.run(`CREATE TABLE matches (
		matchid INTEGER UNIQUE PRIMARY KEY,
		pickupid INTEGER NOT NULL,
		team1list TEXT,
		team2list TEXT,
		result TEXT,
		starttime INTEGER
		)`);
	db.run(`CREATE TABLE elochanges (
		matchid INTEGER NOT NULL,
		discordid TEXT NOT NULL,
		leagueid INTEGER NOT NULL,
		muchange REAL,
		sigchange REAL,
		classes INTEGER,
		PRIMARY KEY (matchid, discordid, leagueid)
		)`);
	db.run(`CREATE TABLE ranks (
		discordid TEXT NOT NULL,
		leagueid INTEGER NOT NULL,
		mu REAL,
		sig REAL,
		wins INTEGER DEFAULT 0,
		losses INTEGER DEFAULT 0,
		draws INTEGER DEFAULT 0,
		streak INTEGER DEFAULT 0,
		PRIMARY KEY (discordid, leagueid)
		)`);
	db.run(`CREATE TABLE leagues (
		id INTEGER UNIQUE PRIMARY KEY,
		name TEXT,
		locked INTEGER DEFAULT 1
		)`);
});

db.close();
