//they are structures because they can hold a lot if things (information) but they don't do anything (objects/machines). They also are NOT read only (records)

function Player(){
	this.nickname = null;
	this.leagues = {};
	this.matchId = null;
	this.punished = false;
	this.queuedAt = null;
	this.idleSince = null;
}

function Pickup(r, g){
	/*
	 * STRUCTURE OF PICKUP:
	 *  - A whole bunch of settings (e.g. mapppool, teamsize, etc)
	 *  - A whole bunch of references (e.g. name, pickupid, leagueid)
	 *  - A whole bunch of stats (all begin with "s_" e.g. s_totalgames)
	 *  - A queue (of players waiting for a game
	 *  - A list of all active matches
	 *
	 *  if you are interested in the SPECIFIC settings/stats/references
	 *  then look at the "createPickup" function in admin.js
	 *  or column names in the pickups table in the database
	 */
	for(property in r){
		if(r[property]===null){
			this[property] = g[property];
		}else{
			switch(property){
				case "mappool":
					this[property] = r[property].split(',');
					break;
				default:
					this[property] = r[property];
					break;
			}
		}
	}
	this.queue = [];
	this.matches = {};
}

function Match(matchid, pickupid, players, state){
	this.matchId = matchid;
	this.pickupId = pickupid;
	this.t1 = [];
	this.t2 = [];
	/* results:
	 * [d]raw, [c]ancled, [1]team won, [2]team won
	*/
	this.result = null;
	this.startTime = Date.now();

	this.scratch = [];
	this.players = players;
	/* States:
	 * 0 = waiting for ready
	 * 1 = pickteams
	 * 2 = waiting for result
	 *
	 */
	this.state = state;
	this.capReport = [null,null];
	this.quality = 0;
	this.winPercent = 0;
	this.drawRate = 0;
	this.map = null;
}

function Guild(r={}){
	for(property in r){
		switch(property){
			case "mappool": case "channelids":
				this[property] = r[property].split(',');
				break;
			default:
				this[property] = r[property];
				break;
		}
	}

	this.active = {};
	this.pickups = {};
}

module.exports = {
	Player,
	Pickup,
	Match,
	Guild
}
